<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tracker;

class TrackerController extends Controller
{
    /**
     * Returns estimated delivery date of the 
     * package tracking code.
     *
     * @return string $deliveryDate
     */
    public function getDeliveryDate(Request $request)
    {
        $trackingCode = $request['trackingCode'];
        $deliveryDate = null;

        if(strtolower(config('app.data_source'))=='csv'){
           $packages = $this->importCsv(); 
           foreach ($packages as $package) {
               if($trackingCode==$package['tracking_code'])
               {
                   $deliveryDate = $package['estimated_delivery_date'];
                   return $deliveryDate;
                }
            }
        return "Package with this tracking code not found!";

        }else{   
            $tracker = Tracker::where('tracking_code', $trackingCode)->first();
            
            if( $tracker == null) {
                return "Package with this tracking code not found!";
            }
            
            $deliveryDate = $tracker->estimated_delivery_date;
            return $deliveryDate;
        }
    }

    /**
     * Reads array and inserts it in our database.
     *
     * @return string $deliveryDate
     */
    public function importCsv()
    {
        $file = public_path('tracking-info.csv');
        $packages = $this->csvToArray($file);
        return $packages;
    }

    /**
     * Reads the CSV file and converts it to an array.
     *
     * @return array $data
     */
    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
}
