<?php

use Faker\Generator as Faker;

$factory->define(App\Tracker::class, function (Tracker $tracker) {
    return [
        'tracking_code' => $tracker->tracking_code,
        'estimated_delivery_date' => $tracker->estimated_delivery_date
    ];
});