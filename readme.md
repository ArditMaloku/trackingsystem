# Package Tracking System 

## Introduction

The project is build on Laravel as a Backend and jQuery library as frontend using Ajax to make a request to the backend, and pull the estimated devlivery.

## Usage


Run the application
   php artisan serve

To change the storage solution go in the config/app.php file and change DATA_SOURCE for CSV or SQLite

To fill SQLite use Tinker 

For CSV storage you will find a file in public/tracking-info.csv

To make unit test of project 
   Run composer update
   Then you add this to you ~/.bash_profile or ~/.profile
   export PATH=~/.composer/vendor/bin:$PATH 